package jF_Client;

import java.awt.event.ActionEvent;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import com.group5.jfs.*;
//import java.net.InetAddress;
//import java.net.InetSocketAddress;
//import java.net.SocketAddress;
//import javax.swing.SwingUtilities;
//import javax.swing.JCheckBoxMenuItem;



/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class LogonFrame extends javax.swing.JFrame {

	{
		//Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8478300066984997079L;
	private JLabel userName_Label;
	private JMenuItem jMenuItem3;
	private JSeparator jSeparator1;
	private JMenuItem lmServer_Current;
	private AbstractAction lmHelp_About;
	private JMenuItem jMenuItem2;
	private AbstractAction lmFile_Exit;
	private JMenuItem jMenuItem1;
	private JMenu logonMenu_Help;
	private JMenu logonMenu_Server;
	private JMenu logonMenu_File;
	private JMenuBar logonMenuBar;
	private AbstractAction logonAbs;
	private JButton logonButton;
	private JTextField username_Text;
	private JButton logon_exit;
	private AbstractAction registerButtonAction;
	private JButton registerButton;
	private AbstractAction logon_Exit;
	private AbstractAction lmFile_debugMode;
	private JTextField password_Text;
	private JLabel CSD_TestSuccess;
	private AbstractAction lmFile_debugAbs;
	private JCheckBoxMenuItem lmFile_Debug;
	private JSeparator jSeparator2;
	private AbstractAction CSD_TestAbs;
	private AbstractAction CSD_OkAbs;
	private AbstractAction CSD_CancelAbs;
	private JButton CSD_TestButton;
	private JButton CSD_CancelButton;
	private JButton CSD_OkButton;
	private JTextField CSD_ServerText;
	private JLabel CSD_titleLabel;
	private JDialog changeServerDialog;
	private AbstractAction lmServer_Change;
	public JLabel error_label;
	private JLabel loginText;
	private JLabel password_label;
		
	/**
	* Auto-generated main method to display this JFrame
	*/
	//Constructor
	public LogonFrame() {
		super();
		initGUI();
	}
	//builds the GUI 
	private void initGUI() {
		try {
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			{
				//Label for the UserName
				userName_Label = new JLabel();
				userName_Label.setText("Username:");
			}
			{
				//Label for the PassWord
				password_label = new JLabel();
				password_label.setText("Password:");
			}
			{
				//Textfield for username
				username_Text = new JTextField();
			}
			{
				//Just the title
				loginText = new JLabel();
				loginText.setText("Please Log into the system");
			}
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addComponent(loginText, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGap(26)
				.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(username_Text, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(userName_Label, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getPassword_Textx(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
				    .addComponent(password_label, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getLogon_exit(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getRegisterButton(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getLogonButton(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				.addComponent(getError_label(), GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(28, Short.MAX_VALUE));
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(thisLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addComponent(getLogon_exit(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, 1, Short.MAX_VALUE)
				        .addComponent(getRegisterButton(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				        .addComponent(getLogonButton(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
				    .addComponent(getError_label(), GroupLayout.Alignment.LEADING, 0, 281, Short.MAX_VALUE)
				    .addGroup(thisLayout.createSequentialGroup()
				        .addGap(42)
				        .addGroup(thisLayout.createParallelGroup()
				            .addGroup(thisLayout.createSequentialGroup()
				                .addGroup(thisLayout.createParallelGroup()
				                    .addComponent(userName_Label, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
				                    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				                        .addComponent(password_label, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
				                        .addGap(6)))
				                .addGap(21)
				                .addGroup(thisLayout.createParallelGroup()
				                    .addGroup(thisLayout.createSequentialGroup()
				                        .addGap(0, 0, Short.MAX_VALUE)
				                        .addComponent(username_Text, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
				                    .addGroup(thisLayout.createSequentialGroup()
				                        .addComponent(getPassword_Textx(), GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
				                        .addGap(0, 0, Short.MAX_VALUE)))
				                .addGap(39))
				            .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				                .addGap(32)
				                .addComponent(loginText, 0, 207, Short.MAX_VALUE)))))
				.addContainerGap());
			this.setTitle("Jfriends: Logon");
			{
				logonMenuBar = new JMenuBar();
				setJMenuBar(logonMenuBar);
				logonMenuBar.add(getLogonMenu_File());
				logonMenuBar.add(getLogonMenu_Server());
				logonMenuBar.add(getLogonMenu_Help());
			}
			pack();
			this.setSize(317, 276);
		} catch (Exception e) {
		    //add your error handling code here
			errorDialog errorD = new errorDialog("Error Creating the logonWindow, try restarting the program");
			errorD.setVisible(true);
			System.out.println("Error Creating the logonWindow");
			e.printStackTrace();
		}
	}
	
	public JTextField getUsername_Text() {
		return username_Text;
	}
	
	public JTextField getPassword_Text() {
		return password_Text;
	}
	
	private JLabel getError_label() {
		if(error_label == null) {
			error_label = new JLabel();
		}
		return error_label;
	}
	

	private JButton getLogon_exit() {
		if(logon_exit == null) {
			logon_exit = new JButton();
			logon_exit.setText("Exit");
			logon_exit.setAction(getLogon_Exit());
		}
		return logon_exit;
	}
	//Exit Button action
	private AbstractAction getLogon_Exit() {
		if(logon_Exit == null) {
			logon_Exit = new AbstractAction("Exit", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4760396260103512745L;

				public void actionPerformed(ActionEvent evt) {
					System.exit(0);
				}
			};
		}
		return logon_Exit;
	}
	
	private JButton getRegisterButton() {
		if(registerButton == null) {
			registerButton = new JButton();
			registerButton.setText("Register");
			registerButton.setAction(getRegisterButtonAction());
		}
		return registerButton;
	}
	//Register button action
	private AbstractAction getRegisterButtonAction() {
		if(registerButtonAction == null) {
			registerButtonAction = new AbstractAction("Register", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 8716651260738472724L;

				public void actionPerformed(ActionEvent evt) {
					String UN = username_Text.getText();
					String pass = password_Text.getText();
					String PW = pass.toString();
					
					Message register;
					register = new Message(UN,"","REGISTER",PW);
					try
					{
						new messageThread(register).start();
					} catch (Exception e)
					{
						error_label.setText("Error, could not Register you");
						System.out.println("ERROR: logonFrame Regster Abstract: new messagethread");
						e.printStackTrace();
					}
				}
			};
		}
		return registerButtonAction;
	}
	
	private JButton getLogonButton() {
		if(logonButton == null) {
			logonButton = new JButton();
			logonButton.setText("Logon");
			logonButton.setAction(getLogonAbs());
		}
		return logonButton;
	}
	private Vector<Group> debugFL ()
	{
		//make a friendsList
		Vector<Group> rawFL = new Vector<Group>();
		Group one = new Group();
		Group two = new Group();
		
		one.groupName = "Debug One";
		two.groupName = "Debug Two";
		
		LinkedList<String> OneFriends = new LinkedList<String>();
		LinkedList<String> TwoFriends = new LinkedList<String>();
		
		for(int i=0; i<=5; i++)
		{
			OneFriends.add("Friend:" + Integer.toString(i));
			TwoFriends.add("Friend:" + Integer.toString(i));
		}
		
		one.friends = OneFriends;
		two.friends = TwoFriends;

		rawFL.add(one);
		rawFL.add(two);
		return rawFL;
	}
	private AbstractAction getLogonAbs() {
		if(logonAbs == null) {
			logonAbs = new AbstractAction("Logon", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 8716651260738472724L;

				public void actionPerformed(ActionEvent evt) {
					//Get the text from the deal
					String UN = username_Text.getText();
					String pass = password_Text.getText();
					
					//String PW = pass.toString();
					System.out.println(pass);
					
					
					if(Main.debug)
					{
						Main.Username = UN;
						Main.isLoggedOn = true;
						Main.buildFriendsList(debugFL());
						Main.Logon.dispose();
					}
					else
					{
						Message Logon = new Message(UN,"","LOGON",pass);
						try
						{
							new messageThread(Logon).start();
						} catch (Exception e)
						{
							error_label.setText("Error, could not Log you on");
							System.out.println("ERROR: LogonFrame: LogonAbstract: could not create new messageThread");
							System.out.println("User:"+UN + "\nPassword:" + pass);
							e.printStackTrace();
						}
					}
					
				}
			};
		}
		return logonAbs;
	}

	private JMenu getLogonMenu_File() {
		if(logonMenu_File == null) {
			logonMenu_File = new JMenu();
			logonMenu_File.setText("File");
			logonMenu_File.add(getLmFile_Debug());
			logonMenu_File.add(getJSeparator2());
			logonMenu_File.add(getJMenuItem1());
			

		}
		return logonMenu_File;
	}
	
	private JMenu getLogonMenu_Server() {
		if(logonMenu_Server == null) {
			logonMenu_Server = new JMenu();
			logonMenu_Server.setText("Server");
			logonMenu_Server.add(getLmServer_Current());
			logonMenu_Server.add(getJSeparator1());
			logonMenu_Server.add(getJMenuItem3());
		}
		return logonMenu_Server;
	}
	
	private JMenu getLogonMenu_Help() {
		if(logonMenu_Help == null) {
			logonMenu_Help = new JMenu();
			logonMenu_Help.setText("Help");
			logonMenu_Help.add(getJMenuItem2());
		}
		return logonMenu_Help;
	}
	
	private JMenuItem getJMenuItem1() {
		if(jMenuItem1 == null) {
			jMenuItem1 = new JMenuItem();
			jMenuItem1.setText("jMenuItem1");
			jMenuItem1.setAction(getLmFile_Exit());
		}
		return jMenuItem1;
	}
	
	private AbstractAction getLmFile_Exit() {
		if(lmFile_Exit == null) {
			lmFile_Exit = new AbstractAction("Exit", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 7280835287768734569L;

				public void actionPerformed(ActionEvent evt) {
					System.exit(0);
				}
			};
		}
		return lmFile_Exit;
	}
	
	private JMenuItem getJMenuItem2() {
		if(jMenuItem2 == null) {
			jMenuItem2 = new JMenuItem();
			jMenuItem2.setText("jMenuItem2");
			jMenuItem2.setAction(getLmHelp_About());
		}
		return jMenuItem2;
	}
	
	private AbstractAction getLmHelp_About() {
		if(lmHelp_About == null) {
			lmHelp_About = new AbstractAction("About", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -7046759337486222620L;

				public void actionPerformed(ActionEvent evt) {
					//TODO add an AboutDialog
					errorDialog errorD = new errorDialog("Nobody here but us chickens!");
					errorD.setVisible(true);
				}
			};
		}
		return lmHelp_About;
	}
	
	private JMenuItem getLmServer_Current() {
		if(lmServer_Current == null) {
			lmServer_Current = new JMenuItem();
			lmServer_Current.setText("Current Server:\n  "+Main.Server);
		}
		return lmServer_Current;
	}
	
	private JSeparator getJSeparator1() {
		if(jSeparator1 == null) {
			jSeparator1 = new JSeparator();
		}
		return jSeparator1;
	}
	
	private JMenuItem getJMenuItem3() {
		if(jMenuItem3 == null) {
			jMenuItem3 = new JMenuItem();
			jMenuItem3.setText("jMenuItem3");
			jMenuItem3.setAction(getLmServer_Change());
		}
		return jMenuItem3;
	}
	
	private AbstractAction getLmServer_Change() {
		if(lmServer_Change == null) {
			lmServer_Change = new AbstractAction("Change", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 7834903042014220036L;

				public void actionPerformed(ActionEvent evt) {
					getChangeServerDialog().pack();
					getChangeServerDialog().setVisible(true);
				}
			};
		}
		return lmServer_Change;
	}
	
	private JDialog getChangeServerDialog() {
		if(changeServerDialog == null) {
			changeServerDialog = new JDialog(this);
			GroupLayout changeServerDialogLayout = new GroupLayout((JComponent)changeServerDialog.getContentPane());
			changeServerDialog.getContentPane().setLayout(changeServerDialogLayout);
			changeServerDialog.setTitle("Change Server");
			changeServerDialog.setPreferredSize(new java.awt.Dimension(378, 230));
			changeServerDialog.setSize(378, 230);
			changeServerDialogLayout.setHorizontalGroup(changeServerDialogLayout.createSequentialGroup()
				.addContainerGap(61, 61)
				.addComponent(getCSD_titleLabel(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(changeServerDialogLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, changeServerDialogLayout.createSequentialGroup()
				        .addComponent(getCSD_TestButton(), 0, 125, Short.MAX_VALUE)
				        .addGap(41))
				    .addGroup(GroupLayout.Alignment.LEADING, changeServerDialogLayout.createSequentialGroup()
				        .addComponent(getCSD_ServerText(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 41, Short.MAX_VALUE))
				    .addGroup(GroupLayout.Alignment.LEADING, changeServerDialogLayout.createSequentialGroup()
				        .addComponent(getCSD_TestSuccess(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 41, Short.MAX_VALUE))
				    .addGroup(GroupLayout.Alignment.LEADING, changeServerDialogLayout.createSequentialGroup()
				        .addPreferredGap(getCSD_TestButton(), getCSD_CancelButton(), LayoutStyle.ComponentPlacement.INDENT)
				        .addComponent(getCSD_CancelButton(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				        .addComponent(getCSD_OkButton(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 0, Short.MAX_VALUE)))
				.addContainerGap(17, 17));
			changeServerDialogLayout.setVerticalGroup(changeServerDialogLayout.createSequentialGroup()
				.addContainerGap(34, 34)
				.addGroup(changeServerDialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getCSD_ServerText(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getCSD_titleLabel(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				.addComponent(getCSD_TestButton(), GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(getCSD_TestSuccess(), GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
				.addGap(32)
				.addGroup(changeServerDialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getCSD_CancelButton(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getCSD_OkButton(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(23, 23));
		}
		return changeServerDialog;
	}
	
	private JLabel getCSD_titleLabel() {
		if(CSD_titleLabel == null) {
			CSD_titleLabel = new JLabel();
			CSD_titleLabel.setText("Change Host Server:");
		}
		return CSD_titleLabel;
	}
	
	private JTextField getCSD_ServerText() {
		if(CSD_ServerText == null) {
			CSD_ServerText = new JTextField();
			CSD_ServerText.setText(Main.Server);
		}
		return CSD_ServerText;
	}
	
	private JButton getCSD_OkButton() {
		if(CSD_OkButton == null) {
			CSD_OkButton = new JButton();
			CSD_OkButton.setText("Ok");
			CSD_OkButton.setAction(getCSD_OkAbs());
		}
		return CSD_OkButton;
	}
	
	private JButton getCSD_CancelButton() {
		if(CSD_CancelButton == null) {
			CSD_CancelButton = new JButton();
			CSD_CancelButton.setText("Cancel");
			CSD_CancelButton.setAction(getCSD_CancelAbs());
		}
		return CSD_CancelButton;
	}
	
	private JButton getCSD_TestButton() {
		if(CSD_TestButton == null) {
			CSD_TestButton = new JButton();
			CSD_TestButton.setText("Test Connection");
			CSD_TestButton.setAction(getCSD_TestAbs());
		}
		return CSD_TestButton;
	}
	
	private AbstractAction getCSD_CancelAbs() {
		if(CSD_CancelAbs == null) {
			CSD_CancelAbs = new AbstractAction("Cancel", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 5331669986853687629L;

				public void actionPerformed(ActionEvent evt) {
					changeServerDialog.dispose();
				}
			};
		}
		return CSD_CancelAbs;
	}
	
	private AbstractAction getCSD_OkAbs() {
		if(CSD_OkAbs == null) {
			CSD_OkAbs = new AbstractAction("Ok", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -6715255965591398994L;

				public void actionPerformed(ActionEvent evt) {
					Main.Server = CSD_ServerText.getText();
					changeServerDialog.dispose();
				}
			};
		}
		return CSD_OkAbs;
	}
	
	private AbstractAction getCSD_TestAbs() {
		if(CSD_TestAbs == null) {
			CSD_TestAbs = new AbstractAction("Test Connection", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 5026275361211173419L;

				public void actionPerformed(ActionEvent evt) {
					//test the connection by seeing if you can connect
					Socket testConnection;
					try
					{
						testConnection = new Socket(CSD_ServerText.getText(),Main.SEND_PORT);
						if(testConnection.isConnected())
						{
							CSD_TestSuccess.setText("Success!");
							testConnection.sendUrgentData(1);
						}
						testConnection.close();
					} catch (Exception e)
					{
						errorDialog errorD = new errorDialog("Error connecting with the server");
						errorD.setVisible(true);
						e.printStackTrace();
						}
				}
			};
		}
		return CSD_TestAbs;
	}

	private AbstractAction getLmFile_debugMode() {
		if(lmFile_debugMode == null) {
			lmFile_debugMode = new AbstractAction("Enable Debug Mode", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -8448842519705449248L;

				public void actionPerformed(ActionEvent evt) {
					//TODO
				}
			};
		}
		return lmFile_debugMode;
	}

	private JSeparator getJSeparator2() {
		if(jSeparator2 == null) {
			jSeparator2 = new JSeparator();
		}
		return jSeparator2;
	}

	private JCheckBoxMenuItem getLmFile_Debug() {
		if(lmFile_Debug == null) {
			lmFile_Debug = new JCheckBoxMenuItem();
			lmFile_Debug.setText("Debug Mode");
			lmFile_Debug.setSelected(false);
			lmFile_Debug.setAction(getLmFile_debugAbs());
		}
		return lmFile_Debug;
	}
	
	private AbstractAction getLmFile_debugAbs() {
		if(lmFile_debugAbs == null) {
			lmFile_debugAbs = new AbstractAction("Debug Mode", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -4020090702548250396L;

				public void actionPerformed(ActionEvent evt) {
					if(lmFile_Debug.getState())
					{
						Main.debug = true;
					}
					else
					{
						Main.debug = false;
					}
				}
			};
		}
		return lmFile_debugAbs;
	}
	
	private JLabel getCSD_TestSuccess() {
		if(CSD_TestSuccess == null) {
			CSD_TestSuccess = new JLabel();
		}
		return CSD_TestSuccess;
	}
	
	private JTextField getPassword_Textx() {
		if(password_Text == null) {
			password_Text = new JTextField();
		}
		return password_Text;
	}

}
