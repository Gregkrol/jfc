package jF_Client;


import java.io.IOException;
import java.io.ObjectInputStream;

import com.group5.jfs.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Enumeration;
import java.util.Vector;



/* JFC Main.java
 * 		 
 * The GUI elements for this project were built with Jigloo GUI builder.
 * 
 * LogonFrame is called first, prompting the user to log in. Then Main goes into a listening While loop, kicking off new threads whenever there is a connection.
 * 
 * LogonFrame will create a messageThread to send a LOGON message when the user hits the 'logon' button.
 * The logon frame will only dispose when a LOGON message comes in with the Data value as true.
 * This message will also build a vector of groups, which are the user's friends in group format.
 * This message will also cause the buddylist to be built and set visible
 * 
 * The buddylist is a menu and jTextPane (non-editable)
 * double clicking a name will cause a new conversation window
 * 
 * To call a new error dialog:
 * errorDialog errorD = new errorDialog("ErrorText goes here");
 * errorD.setVisible(true);
 */
public class Main
{
	//user's handle
	public static String Username = null;
	//users pass, not really needed
	public static String Password = null;
	//did the person authenticate with the server?
	public static boolean isLoggedOn = false;
	//a Group Vector of friends
	public static Vector<Group> friends = new Vector<Group>();
	//the friendsList that friendsList() builds from
	public static String friendsHTML = null;
	//a vector of conversation windows
	public static Vector<ConversationWindow> conversations = new Vector<ConversationWindow>();
	
	//Socket stuff
	//what server?
	public static String Server = "localhost";
	//what port to listen on? (port server sends on)
	public static final int LISTEN_PORT = 9341;
	//what port to send on? (port server listens on)
	public static final int SEND_PORT = 9340;
	//create a serverSocket to listen for messages
	public static ServerSocket serverSocket;
	//OIS
	public static ObjectInputStream ois;
	//socket
	public static Socket Socket;
	
	//GUI stuff
	public static LogonFrame Logon;
	public static FriendsList fl;
	
	//Debugging Stuff
	public static boolean debug = false;
	
	public static void main(String[] args) 
	{
		
		//initialize the serverSocket to the correct port
		try
		{
			serverSocket = new ServerSocket(LISTEN_PORT);
			
		} catch (IOException ioe)
		{
			System.err.println("Could not listen on port " + LISTEN_PORT);
			System.exit(-1);
		}
		
		//generate a logon window
		logon();
		
		// Start listening
		while(true)
		{
			try
			{
				new messageThread(serverSocket.accept()).start(); 
			} catch (IOException ioe)
			{
				System.err.println(ioe.getMessage());
				System.exit(-1);
			}
			
		}

	}
	//this creates a friendsList swing window
	private static void friendsList()
	{
		fl = new FriendsList();
		fl.setLocationByPlatform(true);
		fl.setVisible(true);
	}
	//this creates a LogonFrame window
	public static void logon()
	{
		
		//create logon window
		//A message from the server disposes it
		Logon = new LogonFrame();
		Logon.setVisible(true);
	}
	public static void logoff() throws IOException
	{
		Message logoff = new Message(Username, "", "LOGOFF", "");
		new messageThread(logoff).start();
	}
	//Given a vector of Group, go through and build htm
	public static void buildFriendsList(Vector<Group> rawFL)
	{
		//for refrence
		friends = rawFL;
		System.out.println(friends.size());
		//Begin building the HTML for the  friendsList
		//Declare CSS first
		String css = "<style type=\"text/css\"> Body{padding: 5px; font-family: georgia;} a{padding: 2px; color: #384b7a;} a:hover{color:#d54e21;} div{padding-left: 5px; border: 1px solid black; line-height:1.6;} h1{font-size: 90%;}</style></HEAD><Body>\n";
		String beginHTML = "<HTML><HEAD><Title>FriendsList</Title>" + css;
		//end of the HTML
		String endHTML = "</Body></HTML>";
		//storage for the current div the while loop is working on
		String div;
		//storage for the current contents for the while loop
		String contents = null;
		//add the begning html to the friendslist
		friendsHTML = beginHTML;
		//Temp String
		String groupName;
		//Temp Group
		Group currGroup;
		
		//go through each group and build a Div
		Enumeration<Group> e = rawFL.elements();
		while(e.hasMoreElements())
		{
			
			//BUILD THE HTML
			currGroup = e.nextElement();
			//get the groupName
			groupName = currGroup.groupName;
			System.out.println(groupName);
			//set it as the div ID (for some sweet css)
			div = "<div id=\"" + groupName + "\"><br>"+"\n";
			//display the groupname as a h1
			contents = "<h1>" + groupName + "</h1>\n";
			//get the friends in the group
			for(int i=0; i<currGroup.friends.size(); i++){
				System.out.println(currGroup.friends.get(i));
				contents += "<a href=\"http://" + currGroup.friends.get(i) + "\">" + currGroup.friends.get(i) + "</a><br>\n";
			}
			friendsHTML += div+contents + "</div>\n";
		}
		friendsHTML += endHTML;
		System.out.println(friendsHTML);
		//now that we have a huge string that is the html buddylist, we can buld the buddylist (buddylist automatically builds from Main.friendsHTML)

			friendsList();
	
	}

	//finds a specified conversationWindow
	//it enumerates through the Main vector of CWs and returns the index number if found, else it returns -1
	public static int findConversationWindow(String friend)
	{
		int index = 0;
		Enumeration<ConversationWindow> e = conversations.elements();
		while(e.hasMoreElements()){
			if(e.toString().contains(friend))
			{
				return index;
			}
			else
			{
				index++;
			}
			}
		return -1;
	}
}


