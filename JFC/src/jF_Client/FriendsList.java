package jF_Client;

import java.awt.event.ActionEvent;
import java.util.Enumeration;
import javax.swing.WindowConstants;
import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
//import java.util.Enumeration;
//import javax.swing.SwingUtilities;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.awt.event.MouseListener;
//import javax.swing.text.html.HTMLEditorKit;
//import java.awt.BorderLayout;


import com.group5.jfs.Message;
import com.group5.jfs.Group;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class FriendsList extends javax.swing.JFrame implements HyperlinkListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6538106647018726624L;
	/**
	* Auto-generated main method to display this JFrame
	*/
	private JButton aboutDialog_OK;
	private AbstractAction abstractAction1;
	private AbstractAction menuFile_Exit;
	private JDialog newConversationDialog;
	private JMenuItem jMenuItem4;
	private AbstractAction newDialog_new;
	private AbstractAction newDia_cancel;
	private JSeparator jSeparator2;
	private JSeparator jSeparator1;
	private AbstractAction aord_OKAbs;
	private AbstractAction aord_CancelAbs;
	private JButton aord_Cancel;
	private JButton aord_OK;
	private JTextField aord_Textfield;
	private JLabel aord_Label;
	private JDialog addORrem_dialog;
	private AbstractAction menuF_AddF;
	private JButton convoDialog_cancel;
	private AbstractAction moveFtoG_OkAbs;
	private AbstractAction moveFtoG_CancelAbs;
	private JButton moveFtoG_CancelButton;
	private JTextField newConvoText;
	private JLabel newLabel;
	private JButton newButton;
	private AbstractAction menuF_newConvo;
	private JMenuItem jMenuItem3;
	private JEditorPane friendsEditPane;
	private JMenuItem jMenuItem2;
	private JLabel aboutLabel;
	private JButton moveFtoG_OkButton;
	private JComboBox moveFtoG_Gcombo;
	private JTextField moveFtoG_fText;
	private JLabel moveFtoG_to;
	private JLabel moveFtoG_who;
	private JLabel moveFtoG_titleLabel;
	private JDialog moveFtoG_dialog;
	private AbstractAction menuF_moveFtoG;
	private JMenuItem jMenuItem8;
	private AbstractAction menuF_rFriend;
	private JMenuItem jMenuItem7;
	private AbstractAction menuF_rGroup;
	private JMenuItem jMenuItem6;
	private AbstractAction menuF_addGroup;
	private JMenuItem jMenuItem5;
	private JDialog aboutDialog;
	private AbstractAction menuHelp_About;
	private JMenuItem jMenuItem1;
	private JMenu menu_Help;
	private JMenu menu_Friends;
	private JMenu menu_File;
	private JMenuBar friendsList_Menu;

	public FriendsList() {
		super();	
		initGUI();
	}
	
	private void initGUI() {
		try {
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addComponent(getFriendsEditPane(), 0, 535, Short.MAX_VALUE));
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addComponent(getFriendsEditPane(), 0, 234, Short.MAX_VALUE));
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			{
				friendsList_Menu = new JMenuBar();
				setJMenuBar(friendsList_Menu);
				{
					//Exit
					menu_File = new JMenu();
					friendsList_Menu.add(menu_File);
					menu_File.setText("File");
					menu_File.add(getJMenuItem2());
				}
				{
					menu_Friends = new JMenu();
					friendsList_Menu.add(menu_Friends);
					menu_Friends.setText("Friends");
					menu_Friends.add(getJMenuItem3());
					menu_Friends.add(getJSeparator2());
					menu_Friends.add(getJMenuItem4());
					menu_Friends.add(getJMenuItem5());
					menu_Friends.add(getJMenuItem6());
					menu_Friends.add(getJMenuItem7());
					menu_Friends.add(getJSeparator1());
					menu_Friends.add(getJMenuItem8());

				}
				{
					//about
					menu_Help = new JMenu();
					friendsList_Menu.add(menu_Help);
					menu_Help.setText("Help");
					{
						jMenuItem1 = new JMenuItem();
						menu_Help.add(jMenuItem1);
						jMenuItem1.setText("jMenuItem1");
						jMenuItem1.setAction(getMenuHelp_About());
					}
				}
			}
			//friendsEditPane.setText(Main.friendsHTML);
			this.setTitle(Main.Username);
			pack();
			setSize(250, 600);
			
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}

	private AbstractAction getMenuHelp_About() {
		if(menuHelp_About == null) {
			menuHelp_About = new AbstractAction("About", null) {
				/**
				 * Auto-generated
				 */
				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent evt) {
					getAboutDialog().pack();
					getAboutDialog().setVisible(true);
				}
			};
		}
		return menuHelp_About;
	}
	
	private JDialog getAboutDialog() {
		if(aboutDialog == null) {
			aboutDialog = new JDialog(this);
			GroupLayout aboutDialogLayout = new GroupLayout((JComponent)aboutDialog.getContentPane());
			aboutDialog.getContentPane().setLayout(aboutDialogLayout);
			aboutDialog.setPreferredSize(new java.awt.Dimension(383, 168));
			{
				aboutDialog_OK = new JButton();
				aboutDialog_OK.setText("OK");
				aboutDialog_OK.setAction(getAbstractAction1());
			}
			{
				aboutLabel = new JLabel();
				aboutLabel.setText("Jfriends Client: a client to do interract with the Jfriends Service");
			}
			aboutDialog.setSize(383, 168);
			aboutDialogLayout.setHorizontalGroup(aboutDialogLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(aboutDialogLayout.createParallelGroup()
				    .addComponent(aboutLabel, GroupLayout.Alignment.LEADING, 0, 343, Short.MAX_VALUE)
				    .addGroup(GroupLayout.Alignment.LEADING, aboutDialogLayout.createSequentialGroup()
				        .addGap(0, 269, Short.MAX_VALUE)
				        .addComponent(aboutDialog_OK, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
				        .addGap(15)))
				.addContainerGap());
			aboutDialogLayout.setVerticalGroup(aboutDialogLayout.createSequentialGroup()
				.addContainerGap()
				.addComponent(aboutLabel, 0, 68, Short.MAX_VALUE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 0, GroupLayout.PREFERRED_SIZE)
				.addComponent(aboutDialog_OK, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(21, 21));
		}
		return aboutDialog;
	}
	
	private AbstractAction getAbstractAction1() {
		if(abstractAction1 == null) {
			abstractAction1 = new AbstractAction("OK", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent evt) {
					aboutDialog.dispose();
				}
			};
		}
		return abstractAction1;
	}
	
	private JMenuItem getJMenuItem2() {
		if(jMenuItem2 == null) {
			jMenuItem2 = new JMenuItem();
			jMenuItem2.setText("jMenuItem2");
			jMenuItem2.setAction(getMenuFile_Exit());
		}
		return jMenuItem2;
	}
	
	private AbstractAction getMenuFile_Exit() {
		//MENU item: file -> exit
		if(menuFile_Exit == null) {
			menuFile_Exit = new AbstractAction("Exit", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent evt) {
					System.exit(1);
				}
			};
		}
		return menuFile_Exit;
	}

	private JEditorPane getFriendsEditPane() {
		if(friendsEditPane == null) {
			friendsEditPane = new JEditorPane("text/html", Main.friendsHTML);
			friendsEditPane.setEditable(false);
			friendsEditPane.addHyperlinkListener(this);
		}
		return friendsEditPane;
	}
	public void hyperlinkUpdate(HyperlinkEvent event) {
	    if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
	    	String urlTemp = null;
	    	//get friends name
	    	urlTemp = event.getURL().toString();
	    	String fName = urlTemp.replace("http://", "");
	    	System.out.println(fName);
	        Main.conversations.add(new ConversationWindow(fName,fName,""));
	    }
	  }
	
	private JMenuItem getJMenuItem3() {
		if(jMenuItem3 == null) {
			jMenuItem3 = new JMenuItem();
			jMenuItem3.setText("jMenuItem3");
			jMenuItem3.setAction(getMenuF_newConvo());
		}
		return jMenuItem3;
	}
	//New Conversation Mouse Listener TRUE
	private AbstractAction getMenuF_newConvo() {
		if(menuF_newConvo == null) {
			menuF_newConvo = new AbstractAction("New Conversation", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -885917740805757876L;

				public void actionPerformed(ActionEvent evt) {
					getNewConversationDialog().setVisible(true);
				}
			};
		}
		return menuF_newConvo;
	}
	
	private JDialog getNewConversationDialog() {
		if(newConversationDialog == null) {
			newConversationDialog = new JDialog(this);
			GroupLayout newConversationDialogLayout = new GroupLayout((JComponent)newConversationDialog.getContentPane());
			newConversationDialog.getContentPane().setLayout(newConversationDialogLayout);
			newConversationDialog.setSize(291, 156);
			newConversationDialogLayout.setHorizontalGroup(newConversationDialogLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(newConversationDialogLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, newConversationDialogLayout.createSequentialGroup()
				        .addComponent(getNewLabel(), GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 0, Short.MAX_VALUE)
				        .addComponent(getNewConvoText(), GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
				    .addGroup(GroupLayout.Alignment.LEADING, newConversationDialogLayout.createSequentialGroup()
				        .addGap(0, 37, Short.MAX_VALUE)
				        .addComponent(getConvoDialog_cancel(), GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 24, GroupLayout.PREFERRED_SIZE)
				        .addComponent(getNewButton(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap());
			newConversationDialogLayout.setVerticalGroup(newConversationDialogLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(newConversationDialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getNewConvoText(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getNewLabel(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
				.addGap(21)
				.addGroup(newConversationDialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getNewButton(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getConvoDialog_cancel(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(39, 39));
		}
		return newConversationDialog;
	}
	
	private JButton getNewButton() {
		if(newButton == null) {
			newButton = new JButton();
			newButton.setText("New Conversation!!");
			newButton.setAction(getNewDialog_new());
		}
		return newButton;
	}
	
	private JLabel getNewLabel() {
		if(newLabel == null) {
			newLabel = new JLabel();
			newLabel.setText("Friend's Name:");
		}
		return newLabel;
	}
	
	private JTextField getNewConvoText() {
		if(newConvoText == null) {
			newConvoText = new JTextField();
		}
		return newConvoText;
	}
	
	private JButton getConvoDialog_cancel() {
		if(convoDialog_cancel == null) {
			convoDialog_cancel = new JButton();
			convoDialog_cancel.setText("cancel");
			convoDialog_cancel.setAction(getNewDia_cancel());
		}
		return convoDialog_cancel;
	}
	//Mouse Listener for the cancel button
	private AbstractAction getNewDia_cancel() {
		if(newDia_cancel == null) {
			newDia_cancel = new AbstractAction("Cancel", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -142629968466587869L;

				public void actionPerformed(ActionEvent evt) {
					newConversationDialog.dispose();
				}
			};
		}
		return newDia_cancel;
	}
	//MouseListener for the new conversation button
	private AbstractAction getNewDialog_new() {
		if(newDialog_new == null) {
			newDialog_new = new AbstractAction("New Conversation!!!", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 5466746417169041054L;

				public void actionPerformed(ActionEvent evt) {
					String newConvo;
					newConvo = newConvoText.getText();
					Main.conversations.add(new ConversationWindow(newConvo,newConvo,""));
					getNewConversationDialog().dispose();
				}
			};
		}
		return newDialog_new;
	}
	
	private JMenuItem getJMenuItem4() {
		if(jMenuItem4 == null) {
			jMenuItem4 = new JMenuItem();
			jMenuItem4.setText("jMenuItem4");
			jMenuItem4.setAction(getMenuF_AddF());
		}
		return jMenuItem4;
	}
	//Friends Menu Add Friend Listener
	private AbstractAction getMenuF_AddF() {
		if(menuF_AddF == null) {
			menuF_AddF = new AbstractAction("Add Friend", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4080127937621988395L;

				public void actionPerformed(ActionEvent evt) {
					getAddORrem_dialog("ADDFRIEND").setVisible(true);
				}
			};
		}
		return menuF_AddF;
	}
	//Add or remove dialog
	
	private JDialog getAddORrem_dialog(String Type) {
		if(addORrem_dialog == null) {
			addORrem_dialog = new JDialog(this);
			GroupLayout addORrem_dialogLayout = new GroupLayout((JComponent)addORrem_dialog.getContentPane());
			addORrem_dialog.getContentPane().setLayout(addORrem_dialogLayout);
			addORrem_dialog.setPreferredSize(new java.awt.Dimension(300, 186));
			addORrem_dialog.setSize(300, 186);
			addORrem_dialogLayout.setHorizontalGroup(addORrem_dialogLayout.createSequentialGroup()
				.addContainerGap()
				.addComponent(getAord_Label(), GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
				.addGroup(addORrem_dialogLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, addORrem_dialogLayout.createSequentialGroup()
				        .addComponent(getAord_Cancel(), GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
				        .addGap(42)
				        .addComponent(getAord_OK(), GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
				    .addGroup(addORrem_dialogLayout.createSequentialGroup()
				        .addComponent(getAord_Textfield(), GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(55, Short.MAX_VALUE));
			addORrem_dialogLayout.setVerticalGroup(addORrem_dialogLayout.createSequentialGroup()
				.addContainerGap()
				.addComponent(getAord_Label(), GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
				.addComponent(getAord_Textfield(), GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGap(29)
				.addGroup(addORrem_dialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getAord_Cancel(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getAord_OK(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(33, 33));
		}
		//What type of dialog is this?
		if(Type.equals("ADDFRIEND"))
		{
			this.setTitle("Add New Friend");
		}
		else if (Type.equals("ADDGROUP"))
		{
			this.setTitle("Add New Group");
		}
		else if (Type.equals("REMGROUP"))
		{
			this.setTitle("Remove Group");
		}
		else if (Type.equals("REMFRIEND"))
		{
			this.setTitle("Remove Friend");
		}
		
		return addORrem_dialog;
	}
	
	private JLabel getAord_Label() {
		if(aord_Label == null) {
			aord_Label = new JLabel();
			aord_Label.setText("Name:");
		}
		return aord_Label;
	}
	
	private JTextField getAord_Textfield() {
		if(aord_Textfield == null) {
			aord_Textfield = new JTextField();
		}
		return aord_Textfield;
	}
	
	private JButton getAord_OK() {
		if(aord_OK == null) {
			aord_OK = new JButton();
			aord_OK.setText("OK");
			aord_OK.setAction(getAord_OKAbs());
		}
		return aord_OK;
	}
	
	private JButton getAord_Cancel() {
		if(aord_Cancel == null) {
			aord_Cancel = new JButton();
			aord_Cancel.setText("Cancel");
			aord_Cancel.setAction(getAord_CancelAbs());
		}
		return aord_Cancel;
	}
	//cancel listener
	private AbstractAction getAord_CancelAbs() {
		if(aord_CancelAbs == null) {
			aord_CancelAbs = new AbstractAction("Cancel", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1642579569962354812L;

				public void actionPerformed(ActionEvent evt) {
					addORrem_dialog.dispose();
				}
			};
		}
		return aord_CancelAbs;
	}
	//OK listener, for doing something
	private AbstractAction getAord_OKAbs() {
		if(aord_OKAbs == null) {
			aord_OKAbs = new AbstractAction("OK", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 7246919553975820002L;

				public void actionPerformed(ActionEvent evt) {
					//did the user do something?
					//FUTURE: check if group/friend exists in buddylist
					String data;
					if(aord_Textfield.getText().equals(null))
						return;
					else
						data = aord_Textfield.getText();
					//What type of dialog is this?
					String intent = "";
					if(addORrem_dialog.getTitle().equals("Add New Friend"))
					{
						intent = "ADDFRIEND";
					}
					else if (addORrem_dialog.getTitle().equals("Add New Group"))
					{
						intent = "ADDGROUP";
					}
					else if (addORrem_dialog.getTitle().equals("Remove Group"))
					{
						intent = "REMGROUP";
					}
					else if (addORrem_dialog.getTitle().equals("Remove Friend"))
					{
						intent = "REMFRIEND";
					}
					else
					{
						System.out.println("Error, wtf");
					}
					Message sendMessage = new Message(Main.Username,"",intent,data);
					try
					{
						//try sending the message
						new messageThread(sendMessage).start();
					} catch (Exception e)
					{
						System.out.println("ERROR: aord \n Error sending message to add-rem group-friend");
					}
				}
			};
		}
		return aord_OKAbs;
	}
	
	private JMenuItem getJMenuItem5() {
		if(jMenuItem5 == null) {
			jMenuItem5 = new JMenuItem();
			jMenuItem5.setText("jMenuItem5");
			jMenuItem5.setAction(getMenuF_addGroup());
		}
		return jMenuItem5;
	}
	//MEnu Friends add group
	private AbstractAction getMenuF_addGroup() {
		if(menuF_addGroup == null) {
			menuF_addGroup = new AbstractAction("Add Group", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1232349282142572082L;

				public void actionPerformed(ActionEvent evt) {
					getAddORrem_dialog("ADDGROUP").setVisible(true);;
				}
			};
		}
		return menuF_addGroup;
	}
	
	private JMenuItem getJMenuItem6() {
		if(jMenuItem6 == null) {
			jMenuItem6 = new JMenuItem();
			jMenuItem6.setText("jMenuItem6");
			jMenuItem6.setAction(getMenuF_rGroup());
		}
		return jMenuItem6;
	}
	//Menu Friends remove group
	private AbstractAction getMenuF_rGroup() {
		if(menuF_rGroup == null) {
			menuF_rGroup = new AbstractAction("Remove Group", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4259239687943043462L;

				public void actionPerformed(ActionEvent evt) {
					getAddORrem_dialog("REMGROUP").setVisible(true);
				}
			};
		}
		return menuF_rGroup;
	}
	
	private JMenuItem getJMenuItem7() {
		if(jMenuItem7 == null) {
			jMenuItem7 = new JMenuItem();
			jMenuItem7.setText("jMenuItem7");
			jMenuItem7.setAction(getMenuF_rFriend());
		}
		return jMenuItem7;
	}
	
	private AbstractAction getMenuF_rFriend() {
		if(menuF_rFriend == null) {
			menuF_rFriend = new AbstractAction("Remove Friend", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -2159768532779556123L;

				public void actionPerformed(ActionEvent evt) {
					getAddORrem_dialog("REMFRIEND").setVisible(true);;
				}
			};
		}
		return menuF_rFriend;
	}
	
	private JSeparator getJSeparator1() {
		if(jSeparator1 == null) {
			jSeparator1 = new JSeparator();
		}
		return jSeparator1;
	}

	private JSeparator getJSeparator2() {
		if(jSeparator2 == null) {
			jSeparator2 = new JSeparator();
		}
		return jSeparator2;
	}
	
	private JMenuItem getJMenuItem8() {
		if(jMenuItem8 == null) {
			jMenuItem8 = new JMenuItem();
			jMenuItem8.setText("jMenuItem8");
			jMenuItem8.setAction(getMenuF_moveFtoG());
		}
		return jMenuItem8;
	}
	//Move friend to Group
	private AbstractAction getMenuF_moveFtoG() {
		if(menuF_moveFtoG == null) {
			menuF_moveFtoG = new AbstractAction("Move Friend to Group", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 7893107884908464113L;

				public void actionPerformed(ActionEvent evt) {
					getMoveFtoG_dialog().setVisible(true);
				}
			};
		}
		return menuF_moveFtoG;
	}
	
	private JDialog getMoveFtoG_dialog() {
		if(moveFtoG_dialog == null) {
			moveFtoG_dialog = new JDialog(this);
			GroupLayout moveFtoG_dialogLayout = new GroupLayout((JComponent)moveFtoG_dialog.getContentPane());
			moveFtoG_dialog.getContentPane().setLayout(moveFtoG_dialogLayout);
			moveFtoG_dialog.setSize(354, 336);
			moveFtoG_dialogLayout.setHorizontalGroup(moveFtoG_dialogLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(moveFtoG_dialogLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, moveFtoG_dialogLayout.createSequentialGroup()
				        .addComponent(getMoveFtoG_titleLabel(), GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 12, Short.MAX_VALUE)
				        .addComponent(getMoveFtoG_CancelButton(), GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				        .addComponent(getMoveFtoG_OkButton(), GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
				    .addGroup(moveFtoG_dialogLayout.createSequentialGroup()
				        .addGap(47)
				        .addGroup(moveFtoG_dialogLayout.createParallelGroup()
				            .addGroup(GroupLayout.Alignment.LEADING, moveFtoG_dialogLayout.createSequentialGroup()
				                .addComponent(getMoveFtoG_to(), GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
				                .addGap(12))
				            .addComponent(getMoveFtoG_who(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE))
				        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				        .addGroup(moveFtoG_dialogLayout.createParallelGroup()
				            .addGroup(moveFtoG_dialogLayout.createSequentialGroup()
				                .addComponent(getMoveFtoG_Gcombo(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE))
				            .addGroup(moveFtoG_dialogLayout.createSequentialGroup()
				                .addComponent(getMoveFtoG_fText(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)))
				        .addGap(0, 39, Short.MAX_VALUE)))
				.addContainerGap(23, 23));
			moveFtoG_dialogLayout.setVerticalGroup(moveFtoG_dialogLayout.createSequentialGroup()
				.addContainerGap(20, 20)
				.addComponent(getMoveFtoG_titleLabel(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
				.addGap(36)
				.addGroup(moveFtoG_dialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getMoveFtoG_fText(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getMoveFtoG_who(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(moveFtoG_dialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getMoveFtoG_Gcombo(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getMoveFtoG_to(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(86)
				.addGroup(moveFtoG_dialogLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(getMoveFtoG_OkButton(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(getMoveFtoG_CancelButton(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(60, 60));
		}
		moveFtoG_dialog.setTitle("Move a Friend to a Group");
		return moveFtoG_dialog;
	}
	
	private JLabel getMoveFtoG_titleLabel() {
		if(moveFtoG_titleLabel == null) {
			moveFtoG_titleLabel = new JLabel();
			moveFtoG_titleLabel.setText("Move a friend to a group:");
		}
		return moveFtoG_titleLabel;
	}
	
	private JLabel getMoveFtoG_who() {
		if(moveFtoG_who == null) {
			moveFtoG_who = new JLabel();
			moveFtoG_who.setText("Friend Name:");
		}
		return moveFtoG_who;
	}
	
	private JLabel getMoveFtoG_to() {
		if(moveFtoG_to == null) {
			moveFtoG_to = new JLabel();
			moveFtoG_to.setText("Move to:");
		}
		return moveFtoG_to;
	}
	
	private JTextField getMoveFtoG_fText() {
		if(moveFtoG_fText == null) {
			moveFtoG_fText = new JTextField();
		}
		return moveFtoG_fText;
	}
	
	private JComboBox getMoveFtoG_Gcombo() {
		if(moveFtoG_Gcombo == null) {
			int size = Main.friends.size();
			size = size+1;
			String[] groups = new String[size];
			
			System.out.println(Main.friends.size());
			System.out.println(groups.length);
			
			for(int i = 0; i < size-1; i++)
			{
				System.out.println("index "+ Integer.toString(i)+"..." + Main.friends.get(i).groupName);
				groups[i] = Main.friends.get(i).groupName;
			}
			groups[size] = "New..";
			
			ComboBoxModel moveFtoG_GcomboModel = 
				new DefaultComboBoxModel(
						groups);
			moveFtoG_Gcombo = new JComboBox();
			moveFtoG_Gcombo.setModel(moveFtoG_GcomboModel);
		}
		return moveFtoG_Gcombo;
	}
	
	private JButton getMoveFtoG_OkButton() {
		if(moveFtoG_OkButton == null) {
			moveFtoG_OkButton = new JButton();
			moveFtoG_OkButton.setText("Ok");
			moveFtoG_OkButton.setAction(getMoveFtoG_OkAbs());
		}
		return moveFtoG_OkButton;
	}
	
	private JButton getMoveFtoG_CancelButton() {
		if(moveFtoG_CancelButton == null) {
			moveFtoG_CancelButton = new JButton();
			moveFtoG_CancelButton.setText("Cancel");
			moveFtoG_CancelButton.setAction(getMoveFtoG_CancelAbs());
		}
		return moveFtoG_CancelButton;
	}
	//mouse listener for Cancel button abstract [moveFtoG]
	private AbstractAction getMoveFtoG_CancelAbs() {
		if(moveFtoG_CancelAbs == null) {
			moveFtoG_CancelAbs = new AbstractAction("Cancel", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1306298023436279262L;

				public void actionPerformed(ActionEvent evt) {
					moveFtoG_dialog.dispose();
				}
			};
		}
		return moveFtoG_CancelAbs;
	}
	//move friend abs listener
	private AbstractAction getMoveFtoG_OkAbs() {
		if(moveFtoG_OkAbs == null) {
			moveFtoG_OkAbs = new AbstractAction("Ok", null) {
				/**
				 * 
				 */
				private static final long serialVersionUID = -2179779265570344754L;

				public void actionPerformed(ActionEvent evt) {
					String friendToMove;
					String groupToMoveTo;
					friendToMove = moveFtoG_who.getText();
					groupToMoveTo = moveFtoG_Gcombo.getSelectedItem().toString();
					
					if(groupToMoveTo.equals("New..")){
						//TODO
						errorDialog errorD = new errorDialog("This Function is not supported yet");
						errorD.setVisible(true);
						}
					else
					{
						Message move = new Message(Main.Username,friendToMove,"AF2G",groupToMoveTo);
						try
						{
							//try sending the message
							new messageThread(move).start();
						} catch (Exception e)
						{
							errorDialog errorD = new errorDialog("Whoops, something bad happened..");
							errorD.setVisible(true);
						}
					}
					
				}
			};
		}
		return moveFtoG_OkAbs;
	}

}
