package jF_Client;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

//import javax.swing.JScrollPane;
//import javax.swing.SwingUtilities;
import com.group5.jfs.*;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class ConversationWindow extends javax.swing.JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6607493967042475306L;
	private JButton sendButton;
	private JEditorPane scrollBackPane;
	private JTextArea conversationEdit;
	public String title;
	private AbstractAction abstractAction1;
	public String friendName;

	/**
	* Auto-generated main method to display this JFrame
	*/
		
	public ConversationWindow(String Title,String recipient, String initalBody) {
		super();
		title = Title;
		friendName =  recipient;
		initGUI();
		this.setVisible(true);
		this.setScrollback(friendName, initalBody);
		this.setTitle(title);
	}
	
	private void initGUI() {
		try {
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			{
				sendButton = new JButton();
				sendButton.setText("Send");
				sendButton.setAction(getAbstractAction1());
			}
			{
				conversationEdit = new JTextArea();
				conversationEdit.setBackground(Color.LIGHT_GRAY);
				conversationEdit.setBorder(BorderFactory.createLineBorder(Color.black));
				
			}
			{
				scrollBackPane = new JEditorPane();
				scrollBackPane.setEditable(false);
				scrollBackPane.setBackground(Color.WHITE);
				scrollBackPane.setBorder(BorderFactory.createLineBorder(Color.black));
			}
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addComponent(scrollBackPane, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(conversationEdit, 0, 128, Short.MAX_VALUE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(sendButton, 0, 38, Short.MAX_VALUE)
				.addContainerGap());
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(thisLayout.createParallelGroup()
				    .addComponent(scrollBackPane, GroupLayout.Alignment.LEADING, 0, 412, Short.MAX_VALUE)
				    .addComponent(conversationEdit, GroupLayout.Alignment.LEADING, 0, 412, Short.MAX_VALUE)
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addGap(0, 288, Short.MAX_VALUE)
				        .addComponent(sendButton, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap());
			pack();
			this.setSize(450, 530);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	//adds a new line to the scrollback
	public void setScrollback(String Source, String addition)
	{
		String current = scrollBackPane.getText() + "\n";
		String toBe =  current + Source + ":" + addition;
		scrollBackPane.setText(toBe);
	}
	
	//send button mouselistener
	private AbstractAction getAbstractAction1() {
		if(abstractAction1 == null) {
			abstractAction1 = new AbstractAction("Send", null) {
				/**
				 * Auto Generated UID
				 */
				private static final long serialVersionUID = 6348970157087659501L;

				public void actionPerformed(ActionEvent evt) {
					if(Main.debug)
					{
						setScrollback(Main.Username,conversationEdit.getText());
						conversationEdit.setText(" ");
					}
					else
					{
						//create a new message
						Message sendMessage = new Message(Main.Username,friendName,"MESSAGE",conversationEdit.getText());
						try
						{
							//try sending the message
							new messageThread(sendMessage).start();
						} catch (Exception e)
						{
							setScrollback(Main.Username,"Error sending");
						}
						setScrollback(Main.Username,conversationEdit.getText());
						conversationEdit.setText(" ");
					}
				}
			};
		}
		return abstractAction1;
	}

}
