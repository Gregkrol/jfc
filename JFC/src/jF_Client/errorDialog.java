package jF_Client;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.LayoutStyle;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class errorDialog extends javax.swing.JFrame {
	private JButton okButton;
	private AbstractAction okButton_Abs;
	private JEditorPane errorPanel;
	private JLabel errorLabel;
	private String sError = "No Error Given, consult a nerd";

	/**
	* Auto-generated main method to display this JFrame
	*/
		
	public errorDialog(String Error) {
		super();
		sError = Error;
		initGUI();
	}
	
	private void initGUI() {
		try {
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			{
				okButton = new JButton();
				okButton.setText("Ok");
				okButton.setAction(getOkButton_Abs());
			}
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addComponent(getErrorLabel(), GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(getErrorPanel(), GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
				.addGap(0, 22, Short.MAX_VALUE)
				.addComponent(okButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addContainerGap());
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(thisLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addComponent(getErrorLabel(), 0, 39, Short.MAX_VALUE)
				        .addGap(263)
				        .addComponent(okButton, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
				    .addComponent(getErrorPanel(), GroupLayout.Alignment.LEADING, 0, 360, Short.MAX_VALUE))
				.addContainerGap());
			this.setBackground(Color.WHITE);
			this.setTitle("D'oh!");
			pack();
			setSize(400, 300);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	private AbstractAction getOkButton_Abs() {
		if(okButton_Abs == null) {
			okButton_Abs = new AbstractAction("Ok", null) {
				public void actionPerformed(ActionEvent evt) {
					dispose();
				}
			};
		}
		return okButton_Abs;
	}
	
	private JLabel getErrorLabel() {
		if(errorLabel == null) {
			errorLabel = new JLabel();
			errorLabel.setForeground(Color.RED);
			errorLabel.setText("Error:");
		}
		return errorLabel;
	}
	
	private JEditorPane getErrorPanel() {
		if(errorPanel == null) {
			errorPanel = new JEditorPane();
			errorPanel.setBorder(BorderFactory.createLineBorder(Color.black));
			errorPanel.setAlignmentY(TOP_ALIGNMENT);
			errorPanel.setAlignmentX(CENTER_ALIGNMENT);
			errorPanel.setEditable(false);
			errorPanel.setText(sError);
		}
		return errorPanel;
	}

}
