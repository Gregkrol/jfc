package jF_Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Vector;
import com.group5.jfs.*;
//import java.net.SocketAddress;
//import java.net.InetAddress;
//import java.net.InetSocketAddress;

public class messageThread extends Thread
{
	// The socket to use
	private Socket socket;
	// The message passed to the thread
	private Message message = null;
	// The user this thread is for
	private ObjectInputStream ois;
	// The output stream for this thread
	private ObjectOutputStream oos;
	//The host to send to in String form
	private String Host = Main.Server;
	
	//This constructor is used for the ServerSocket in Main
	public messageThread(Socket socket)
	{
		super("JFC_Thread");
		this.socket = socket;
		try
		{
			socket.setKeepAlive(true);
		} catch (SocketException se)
		{
			System.out.println("Error connecting to recieve message");
			System.err.println(se.getMessage());
			se.printStackTrace();
		}
	}
	
	//This constructor is used for sending a message
	public messageThread(Message messageOUT)
	{
		super("JFC_Thread");
		try
		{
			socket = new Socket(Host, Main.SEND_PORT);
		} catch (IOException e)
		{
			errorDialog errorD = new errorDialog(e.getCause().toString());
			errorD.setVisible(true);
			System.out.println("Error connecting to send Message");
			e.printStackTrace();
		}
		message = messageOUT;
	}
	
	//the thread's 'main'
	public void run()
	{
		//if we already have a message, send the message and return
		if(message != null){
			//For debugging console out:
			System.out.println("Sending Message:\n Sender:" + message.sender + "\tRecipient:" + message.recipient + "\tINTENT:" + message.intent + "\tData:" + message.data);
			
			sendMessage(message);
			try
			{
			Thread.sleep(1000); // do nothing for 1000 miliseconds (1 second)
			}
			catch(InterruptedException e)
			{
			e.printStackTrace();
			}
			try
			{
				try
				{
				Thread.sleep(1000); // do nothing for 1000 miliseconds (1 second)
				}
				catch(InterruptedException e)
				{
				e.printStackTrace();
				}
				socket.close();
			} catch (IOException e)
			{
				//for debugging:
				System.out.println("Error closing socket when sending message");
				e.printStackTrace();
			}
			return;
		}
		
		//TODO: Remove some more debugging stuff...
		System.out.println("Accept Thread Started");
		
		// Start the object stream for this socket
		try
		{
			ois = new ObjectInputStream(socket.getInputStream());
		} catch (IOException ioe)
		{
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
		while(socket.isConnected())
		{
			try
			{
				// Read the message from the socket
				message = (Message) ois.readObject();
				try
				{
				Thread.sleep(1000); // do nothing for 1000 miliseconds (1 second)
				}
				catch(InterruptedException e)
				{
				e.printStackTrace();
				}
				ois.close();
			} catch (IOException ioe)
			{
				System.err.print(ioe.getMessage());
				ioe.printStackTrace();
			} catch (ClassNotFoundException cnfe)
			{
				System.err.println(cnfe.getMessage());
				cnfe.printStackTrace();
			}
			
			//For debugging console out:
			System.out.println("Sending Message:\n Sender:" + message.sender + "\tRecipient:" + message.recipient + "\tINTENT:" + message.intent + "\tData:" + message.data);
			
			// Check the message intent and perform the relevant action
			if(message.intent.equals("LOGIN"))
			{
				if(message.data.equals("true")){
					Main.isLoggedOn = true;
					Main.Logon.dispose();
					Main.Username = message.recipient;
					Main.Password = "";
					
				}
				else if(message.data.equals("false")){
					//error, the system could not log you on
					System.out.println("ERROR: The system could not log you on");
				}
				else
					return;
					

			}
			else if(message.intent.equals("FRIENDSLIST"))
			{
				System.out.println("Get Friends List");
				Vector<Group> friendVector = new Vector<Group>();
				try
				{
					friendVector = (Vector<Group>) ois.readObject();
				} catch (IOException e)
				{
					e.printStackTrace();
				} catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				}
				
				Main.buildFriendsList(friendVector);
			}
			else if(message.intent.equals("MESSAGE"))
			{
				System.out.println("Message IN");
				//Message received
				//determine  if there is a conversationn window open,if not then make a new one
				int vWin = Main.findConversationWindow(message.sender);
				if (vWin == -1){
					//new conversation Window here
					Main.conversations.add(new ConversationWindow(message.sender,message.sender, message.data));
				}
				else{
					Main.conversations.get(vWin).setScrollback(message.sender, message.data);
				}
			}
		}
		
		try
		{
			try
			{
			Thread.sleep(1000); // do nothing for 1000 miliseconds (1 second)
			}
			catch(InterruptedException e)
			{
			e.printStackTrace();
			}
			socket.close();
		} catch (IOException ioe)
		{
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
	}
	
	private void sendMessage(Message Message)
	{
		//Send a message
		try
		{
			oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject((Message) message);  	
			oos.flush();
			try
			{
			Thread.sleep(1000); // do nothing for 1000 miliseconds (1 second)
			}
			catch(InterruptedException e)
			{
			e.printStackTrace();
			}
			oos.close();
		} catch (IOException ioe)
		{
			System.out.println("Error execuitng sendMessage");
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
	}
	
}
